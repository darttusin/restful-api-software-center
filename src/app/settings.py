from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    db_host: str = "localhost"
    db_port: int = 3306
    db_user: str = "rootuser"
    db_password: str = "secret2"
    db_name: str = "service"
    minutes_to_expire_token: int = 10080
    secret_key: str = "GENERATEDSECRETKEY"
    algorithm: str = "HS256"

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()  # type: ignore
