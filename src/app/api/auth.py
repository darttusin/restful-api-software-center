from datetime import datetime, timedelta, timezone
from loguru import logger

from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy.ext.asyncio import AsyncSession
from .user.models import UserSchema
from .user.controllers import get_user
from ..settings import settings
from ..external.mysql import get_session

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/v1/user/token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def hash_password(password: str) -> str:
    return pwd_context.hash(password)


async def authenticate_user(
    session: AsyncSession, user_schema: OAuth2PasswordRequestForm
) -> str:
    user_db = await get_user(session, user_schema.username)
    if user_db is None:
        raise HTTPException(404, "user not found")

    if not verify_password(user_schema.password, user_db.password):  # type: ignore
        raise HTTPException(400, "user password incorrect")

    return user_db.id  # type: ignore


async def create_access_token(data: dict) -> str:
    to_encode = data.copy()
    expire = datetime.now(timezone.utc) + timedelta(
        minutes=settings.minutes_to_expire_token
    )
    to_encode["exp"] = expire
    return jwt.encode(to_encode, settings.secret_key, algorithm=settings.algorithm)


async def get_current_user(
    session: AsyncSession = Depends(get_session), token: str = Depends(oauth2_scheme)
) -> UserSchema:
    try:
        payload = jwt.decode(
            token, settings.secret_key, algorithms=[settings.algorithm]
        )
        username = payload.get("sub", None)
        if username is None:
            raise HTTPException(
                401,
                "could not validate credentials",
            )

    except JWTError:
        raise HTTPException(
            401,
            "could not validate credentials",
        )

    user = await get_user(session, username)
    if user is None:
        logger.error(f'msg="user not found" {username=}')
        raise HTTPException(404, "user not found")
    else:
        user = user.__dict__

    user["session"] = session
    return UserSchema(**user)
