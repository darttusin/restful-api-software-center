from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession

from .models import (
    ProfileUserResponse,
    ErrorMessage,
    ProfileUserResponseResult,
    UserResponse,
    NewUser,
)

from .controllers import create_user, get_user_info_by_id
from ..auth import create_access_token, hash_password, authenticate_user
from ...external.mysql import get_session


user_router: APIRouter = APIRouter(
    prefix="/api/v1",
    tags=["user"],
)


@user_router.post(
    path="/user",
    name="Create user",
    response_model=UserResponse,
    status_code=200,
    responses={
        409: {"description": "User already exists", "model": ErrorMessage},
    },
)
async def registration(
    user_schema: NewUser, session: AsyncSession = Depends(get_session)
) -> UserResponse:
    user_schema.password = hash_password(user_schema.password)
    user_id = await create_user(session, user_schema)
    access_token = await create_access_token({"sub": user_schema.login.lower()})

    return UserResponse(id=user_id, access_token=access_token)


@user_router.post(
    path="/user/token",
    name="get token",
    response_model=UserResponse,
    status_code=200,
    responses={
        400: {"description": "User password incorrect", "model": ErrorMessage},
        404: {"description": "User not found", "model": ErrorMessage},
    },
)
async def login(
    user_schema: OAuth2PasswordRequestForm = Depends(),
    session: AsyncSession = Depends(get_session),
) -> UserResponse:
    user_schema.username = user_schema.username.lower()
    user_id = await authenticate_user(session, user_schema)
    access_token = await create_access_token({"sub": user_schema.username})

    return UserResponse(id=user_id, access_token=access_token)


@user_router.get(
    path="/user",
    name="Get user page",
    response_model=ProfileUserResponse,
    status_code=200,
    responses={
        401: {"description": "Could not validate credentials", "model": ErrorMessage},
        404: {"description": "User not found", "model": ErrorMessage},
    },
)
async def profile(
    id: str, session: AsyncSession = Depends(get_session)
) -> ProfileUserResponse:
    user_info = await get_user_info_by_id(session, id)
    if user_info:
        return ProfileUserResponse(
            result=ProfileUserResponseResult(
                id=user_info.id,  # type: ignore
                name=user_info.name,  # type: ignore
                surname=user_info.surname,  # type: ignore
                birthday_date=user_info.birthday_date,  # type: ignore
            )
        )
    else:
        raise HTTPException(404, "user not found")
