from fastapi import HTTPException

from sqlalchemy import or_, select
from sqlalchemy.ext.asyncio.session import AsyncSession
from sqlalchemy.exc import IntegrityError
from .models import NewUser

from ...external.mysql import User


async def create_user(session: AsyncSession, user_schema: NewUser) -> str:
    user_model = User(**user_schema.model_dump())

    session.add(user_model)
    try:
        await session.commit()
    except IntegrityError:
        raise HTTPException(409, "user_name already exists")

    await session.refresh(user_model)
    return str(user_model.id)


async def get_user(session: AsyncSession, login: str) -> User | None:
    result = await session.execute(select(User).where(or_(User.login == login)))
    return result.scalar_one_or_none()


async def get_user_info_by_id(session: AsyncSession, id: str) -> User | None:
    result = await session.execute(select(User).where(User.id == id))
    return result.scalar_one_or_none()
