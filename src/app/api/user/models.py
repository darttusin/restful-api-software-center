from fastapi import HTTPException
from pydantic import BaseModel, field_validator
from sqlalchemy.ext.asyncio import AsyncSession
from datetime import datetime


class NewUser(BaseModel):
    login: str
    password: str
    name: str
    surname: str
    birthday_date: datetime | None = None

    @field_validator("surname")
    def validate_surname(cls, value: str) -> str:
        if len(value) > 30:
            raise HTTPException(400, "surname len more then 30")
        return value

    @field_validator("name")
    def validate_name(cls, value: str) -> str:
        if len(value) > 30:
            raise HTTPException(400, "name len more then 30")
        return value


class UserResponse(BaseModel):
    id: str
    access_token: str
    token_type: str = "bearer"


class UserSchema(BaseModel):
    id: str
    name: str
    surname: str
    birthday_date: datetime | None
    session: AsyncSession

    class Config:
        arbitrary_types_allowed = True


class ProfileUserResponseResult(BaseModel):
    id: str
    name: str
    surname: str
    birthday_date: datetime | None


class ProfileUserResponse(BaseModel):
    error: str | None = None
    result: ProfileUserResponseResult


class ErrorMessage(BaseModel):
    detail: str
