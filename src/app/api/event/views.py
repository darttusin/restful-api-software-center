from fastapi import APIRouter, Depends, HTTPException

from .models import (
    NewEvent,
    NewEventResponse,
    NewEventResponseResult,
    EventResponse,
    Event,
    DefaultResponse,
    ErrorMessage,
)

from ..user.models import UserSchema
from ..auth import get_current_user

from .controllers import (
    create_event,
    get_all_events,
    check_event_creator_id,
    delete_event,
    take_part_event,
    remove_part_event,
)

event_router: APIRouter = APIRouter(
    prefix="/api/v1",
    tags=["event"],
)


@event_router.post(
    path="/event", name="Create event", status_code=200, response_model=NewEventResponse
)
async def new_event(
    event: NewEvent, user: UserSchema = Depends(get_current_user)
) -> NewEventResponse:
    return NewEventResponse(
        result=NewEventResponseResult(
            **(await create_event(user.session, event, user.id)).__dict__
        )
    )


@event_router.get(
    path="/event/list",
    name="Get event list",
    response_model=EventResponse,
    status_code=200,
)
async def event_list(user: UserSchema = Depends(get_current_user)):
    return EventResponse(
        result=[
            Event(**event_dict)
            for event_dict in await get_all_events(user.session, user.id)
        ]
    )


@event_router.delete(
    path="/event",
    name="Delete event",
    response_model=DefaultResponse,
    status_code=200,
    responses={
        403: {"description": "Not user event", "model": ErrorMessage},
    },
)
async def del_event(id: str, user: UserSchema = Depends(get_current_user)):
    if await check_event_creator_id(user.session, id, user.id):
        await delete_event(user.session, id)
        return {"error": None, "result": "event deleted"}
    else:
        raise HTTPException(403, "not your event")


@event_router.post(
    path="/event/part",
    name="Take part in event",
    response_model=DefaultResponse,
    responses={
        404: {"description": "No event with such id", "model": ErrorMessage},
        400: {
            "description": "User already in take part in event",
            "model": ErrorMessage,
        },
    },
    status_code=200,
)
async def take_part(id: str, user: UserSchema = Depends(get_current_user)):
    await take_part_event(user.session, id, user.id)
    return {"error": None, "result": "user added to participants"}


@event_router.delete(
    path="/event/part",
    name="Leave event",
    response_model=DefaultResponse,
    status_code=200,
    responses={
        404: {"description": "No event with such id", "model": ErrorMessage},
        400: {
            "description": "User already in take part in event",
            "model": ErrorMessage,
        },
    },
)
async def leave_event(id: str, user: UserSchema = Depends(get_current_user)):
    await remove_part_event(user.session, id, user.id)
    return {"error": None, "result": "user leave event"}
