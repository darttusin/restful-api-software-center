from datetime import datetime
from fastapi import HTTPException
from pydantic import BaseModel, field_validator


class NewEvent(BaseModel):
    title: str
    text: str

    @field_validator("title")
    def validate_title(cls, title: str) -> str:
        if len(title) > 50:
            raise HTTPException(400, "title len more then 50")
        return title

    @field_validator("text")
    def validate_text(cls, text: str) -> str:
        if len(text) > 2000:
            raise HTTPException(400, "text len more then 2000")
        return text


class NewEventResponseResult(BaseModel):
    id: str
    title: str
    text: str
    creation_date: datetime


class NewEventResponse(BaseModel):
    error: str | None = None
    result: NewEventResponseResult


class EventUser(BaseModel):
    id: str
    name: str
    surname: str
    registration_date: datetime


class Event(BaseModel):
    id: str
    title: str
    text: str
    creation_date: datetime
    creator: EventUser
    participants: list[EventUser]

    def __init__(self, **data):
        data["creator"] = EventUser(**data["creator"].__dict__)
        data["participants"] = [
            EventUser(**participant.__dict__) for participant in data["participants"]
        ]

        super().__init__(**data)


class EventResponse(BaseModel):
    error: str | None = None
    result: list[Event]


class DefaultResponse(BaseModel):
    error: str | None = None
    result: str


class ErrorMessage(BaseModel):
    detail: str
