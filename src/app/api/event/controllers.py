from fastapi import HTTPException
from sqlalchemy.ext.asyncio.session import AsyncSession
from sqlalchemy import delete, select
from sqlalchemy.orm import selectinload


from .models import NewEvent

from ...external.mysql import Event, User


async def create_event(
    session: AsyncSession, event: NewEvent, creator_id: str
) -> Event:
    dump = event.model_dump()
    dump["creator_id"] = creator_id

    model = Event(**dump)
    session.add(model)
    await session.commit()

    await session.refresh(model)
    return model


async def get_all_events(session: AsyncSession, creator_id: str) -> list[dict]:
    stmnt = (
        select(Event)
        .where(Event.creator_id != creator_id)
        .options(selectinload(Event.participants), selectinload(Event.creator))
    )
    cursor = await session.execute(stmnt)

    events = []
    for row in cursor:
        events.append(row[0].__dict__)

    return events


async def check_event_creator_id(
    session: AsyncSession, event_id: str, creator_id: str
) -> bool:
    stmnt = select(Event.creator_id).where(Event.id == event_id)
    result = await session.execute(stmnt)
    result = result.scalar_one_or_none()

    if result is None:
        raise HTTPException(404, "no event with such id")
    else:
        return result == creator_id


async def delete_event(session: AsyncSession, id: str) -> None:
    await session.execute(delete(Event).where(Event.id == id))
    await session.commit()


async def take_part_event(session: AsyncSession, id: str, user_id: str) -> None:
    stmnt = (
        select(Event).where(Event.id == id).options(selectinload(Event.participants))
    )
    cursor = await session.execute(stmnt)
    event = cursor.scalar_one_or_none()
    if event is None:
        raise HTTPException(404, "event not found")

    if event.creator_id == user_id:  # type: ignore
        raise HTTPException(400, "creator can't take part")

    stmnt = select(User).where(User.id == user_id)
    cursor = await session.execute(stmnt)
    user = cursor.scalar_one_or_none()

    if user not in event.participants:
        event.participants.append(user)
        await session.commit()
    else:
        raise HTTPException(400, "user already take part in this event")


async def remove_part_event(session: AsyncSession, id: str, user_id: str) -> None:
    stmnt = (
        select(Event).where(Event.id == id).options(selectinload(Event.participants))
    )
    cursor = await session.execute(stmnt)
    event = cursor.scalar_one_or_none()
    if event is None:
        raise HTTPException(404, "event not found")

    stmnt = select(User).where(User.id == user_id)
    cursor = await session.execute(stmnt)
    user = cursor.scalar_one_or_none()

    if user in event.participants:
        event.participants.remove(user)
        await session.commit()
    else:
        raise HTTPException(400, "user didnot take part in this event")
