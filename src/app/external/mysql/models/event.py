from typing import List
from ..connection import base
from sqlalchemy import Column, String, DateTime, ForeignKey, Table
from sqlalchemy.orm import Mapped, relationship
from uuid import uuid4
from datetime import datetime as dt
from .user import User


event_participants = Table(
    "event_participants",
    base.metadata,
    Column("event_id", ForeignKey("events.id")),
    Column("participant_id", ForeignKey("users.id")),
)


class Event(base):
    __tablename__ = "events"

    id = Column(String(36), primary_key=True, default=uuid4)
    title = Column(String(50), nullable=False)
    text = Column(String(2000), nullable=False)
    creation_date = Column(DateTime, nullable=False, default=dt.now)
    creator_id = Column(String(36), ForeignKey("users.id"))

    creator: Mapped[User] = relationship("User", backref="creator")
    participants: Mapped[List[User]] = relationship(secondary=event_participants)
