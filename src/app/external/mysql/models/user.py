from ..connection import base
from sqlalchemy import Column, String, DateTime
from uuid import uuid4
from datetime import datetime as dt


class User(base):
    __tablename__ = "users"

    id = Column(String(36), primary_key=True, default=uuid4)
    login = Column(String(30), nullable=False, unique=True)
    password = Column(String(60), nullable=False)
    name = Column(String(30), nullable=False)
    surname = Column(String(30), nullable=False)
    registration_date = Column(DateTime, nullable=False, default=dt.now)
    birthday_date = Column(DateTime)
