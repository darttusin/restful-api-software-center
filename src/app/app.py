import time
from typing import Any, Callable

from .external.mysql import create_tables
from .api.user import user_router
from .api.event import event_router
from fastapi import FastAPI, Request
from loguru import logger

app = FastAPI(
    title="RESTful API",
    version="0.0.1",
    docs_url="/api/docs",
)


@app.on_event("startup")
async def on_startup_event():
    await create_tables()


@app.middleware("http")
async def add_process_time_header(request: Request, call_next: Callable) -> Any:
    start_time = time.time()
    response = await call_next(request)

    logger.info(
        f'method="{request.scope["method"]}" router="{request.scope["path"]}" '
        f"process_time={round(time.time() - start_time, 3)} "
        f"status_code={response.status_code}"
    )
    return response


@app.get(path="/api/ping", name="Ping api", status_code=200)
async def ping() -> str:
    return "ok"


app.include_router(user_router)
app.include_router(event_router)
